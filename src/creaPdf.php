<?php
//parte di codice per scaricare index.html
// reference the Dompdf namespace
use Dompdf\Dompdf;

//crea il file con i contenuti di index.html
$pagina=file_get_contents("http://rizzi.bearzi.info/document/public/index.html");
// instantiate and use the dompdf class
$dompdf = new Dompdf();
$dompdf->loadHtml($pagina);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'landscape');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
$dompdf->stream("pagina.pdf");

?>