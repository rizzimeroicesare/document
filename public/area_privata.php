<?php
include('../vendor/autoload.php');
include('../src/config.php');
include('../src/template.php');
$db = newAdoConnection('mysqli');
//$db->debug = true;
$db->connect($dbHost, $dbUsername, $dbPassword, $dbName);


//controllo se è già entrato
session_start();
if($_SESSION['accesso']==false){
	header('Location:http://rizzi.bearzi.info/document/public/login.php');
}

$action=(isset($_REQUEST['action']))?$_REQUEST['action']:'user-list';

switch ($action) {
	case 'user-list':
		include('../src/utenti.php');
		listaUtenti($db);
		break;
	case 'user-delete':
		include('../src/utenti.php');
		cancellaUtente($db,$_REQUEST['id']);
		break;	
	case 'user-add-step-1':
		include('../src/utenti.php');
		formAddUtente();
		break;	
	case 'user-add-step-2':
		include('../src/utenti.php');
		saveUtente($db);
		break;	
	case 'user-modify-step-1':
		include('../src/utenti.php');
		formModifyUtente($db,$_REQUEST['id']);
		break;						
	case 'user-modify-step-2':
		include('../src/utenti.php');
		formModifyUtenteStep2($db,$_REQUEST['id']);
		break;			
}
$action_categorie=(isset($_REQUEST['action']))?$_REQUEST['action']:'category-list';
switch($action_categorie){
	case 'category-list':
		include('../src/categorie.php');
		listaCategorie($db);
		break;
	case 'category-delete':
		include('../src/categorie.php');
		cancellaCategoria($db,$_REQUEST['id']);
		break;	
}


$action_pagine=(isset($_REQUEST['action']))?$_REQUEST['action']:'page-list';
switch($action_pagine){
	case 'page-list':
		include('../src/pagine.php');
		listaPagine($db);
		break;
	case 'page-delete':
		include('../src/pagine.php');
		cancellaPagina($db,$_REQUEST['id']);
		break;	
}