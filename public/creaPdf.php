<?php
include('../vendor/autoload.php');
//parte di codice per scaricare index.html
// reference the Dompdf namespace
use Dompdf\Dompdf;

//crea il file con i contenuti di index.html
$pagina=file_get_contents("index.html");
// instantiate and use the dompdf class
$dompdf = new Dompdf();
$dompdf->loadHtml($pagina);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'landscape');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
$dompdf->stream("pdfGenerato");

                                                //<a href="creaPdf.php" id="carica_pdf">Clicca qui per scaricare pdf</a>
                                                //per mettere il comando nel pdf
?>