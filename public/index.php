<?php
session_start();
?>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {
  font-family: "Lato", sans-serif;
}

.footer {
text-align : right;
position: absolute;
bottom: 0;
}

.circular-square {
    width: 250px;
    height: 250px;
    border-radius: 50%;
}

.centrato {
	display: flex;
	align-items: center;
	justify-content: center;
	height: 100px;
	border: 5px solid black;
}

.sidebar {
  height: 100%;
  width: 0;
  position: fixed;
  z-index: 1;
  top: 0;
  left: 0;
  background-color: #111;
  overflow-x: hidden;
  transition: 0.5s;
  padding-top: 60px;
}

.sidebar a {
  padding: 8px 8px 8px 32px;
  text-decoration: none;
  font-size: 25px;
  color: #818181;
  display: block;
  transition: 0.3s;
}

.sidebar a:hover {
  color: #f1f1f1;
}

.sidebar .closebtn {
  position: absolute;
  top: 0;
  right: 25px;
  font-size: 36px;
  margin-left: 50px;
}

.openbtn {
  font-size: 20px;
  cursor: pointer;
  background-color: #111;
  color: white;
  padding: 10px 15px;
  border: none;
}

.openbtn:hover {
  background-color: #444;
}

#main {
  transition: margin-left .5s;
  padding: 16px;
}

@media screen and (max-height: 450px) {
  .sidebar {padding-top: 15px;}
  .sidebar a {font-size: 18px;}
}

#carica_pdf{
      background-color: #4CAF50; /* Green */
      border: none;
      color: white;
      position: fixed;
      right: 0%;
    }

  #accesso_area_privata{
    background-color: black; 
    border: none;
    color: white;
    position: fixed;
    right: 0%;
  }
</style>
</head>
<body>
  <a href="creaPdf.php" id="carica_pdf">Clicca qui per scaricare pdf</a>
  <br>
  <a href="http://rizzi.bearzi.info/document/public/area_privata.php" id="accesso_area_privata">Clicca qui per accedere all'area privata</a>

<div id="mySidebar" class="sidebar">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&#215;</a>
  <img class="circular-square" src="http://rizzi.bearzi.info/document/docs/logo.png" />
  <dl>
    <dt><a href="#">-Categoria 1</a></dt>
    <dd><a href="#">-pagina 1.1</a></dd>
    <dd><a href="#">-pagina 1.2</a></dd>
    <dt><a href="#">-Categoria 2</a></dt>
    <dd><a href="#">-pagina 2.1</a></dd>
    <dd><a href="#">-pagina 2.2</a></dd>
  </dl>
  <footer><a href="http://rizzi.bearzi.info/document/public/login.php">Accedi</a></footer>
</div>

<div id="main">
  <button class="openbtn" onclick="openNav()">&#9776; Menu</button>  
  <h2 class=centrato>Pagina Web</h2>
  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>
</div>

<script>
function openNav() {
  document.getElementById("mySidebar").style.width = "250px";
  document.getElementById("main").style.marginLeft = "250px";
}

function closeNav() {
  document.getElementById("mySidebar").style.width = "0";
  document.getElementById("main").style.marginLeft= "0";
}
</script>
   
</body>
</html> 
