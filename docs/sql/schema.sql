CREATE TABLE utenti(
	id int AUTO_INCREMENT PRIMARY KEY,
	Nome varchar(255) NOT NULL,
	Cognome varchar(255) NOT NULL,
	Email varchar(255) NOT NULL,
	psword varchar(255) NOT NULL
);

CREATE TABLE catalogo(
	id int AUTO_INCREMENT PRIMARY KEY,
	Categoria int NOT NULL,
	Nome varchar(255) NOT NULL
);

CREATE TABLE pagine(
	id int AUTO_INCREMENT PRIMARY KEY,
	NomePagina varchar(60) NOT NULL,
	Testo varchar(255),
	idCategoria int NOT NULL
)